/**1) У JS існує 8 типи даних : 
 * -number - чисельні дані обмежені ±2^53 (12);
 * -bigInt - чисельні дані довільної довжини;
 * -string - строкові дані ("12");
 * -boolean - бульові (логічні) дані (true/false);
 * -null - окремий тип, що означає (невідоме, порожнє,нульове)
 * -undefined - означає, змінній не було присвоєно значення;
 * -object - особливий тип для зберігання даних, або складніших об'єктів;
 * -symbol - для унікальних ідентифікаторів;
 * 
 * 2) не суворе порівняння == приводить операндів різних типів до числа, 
 * а суворе порівняння === порівнює без приведення типів;
 * 
 * 3)Оператор - елемент мови, що описує дію, яку треба виконати. Існують
 *  оператори порівняння, логічні оператори, умовний оператор if.
 */


let userName = prompt('What is your name');
let userAge = +prompt('How old are You?');
let confirmAction;

while (!userName) {
 userName = prompt('What is your name', userName)
 
}
while (!userAge || typeof userAge !== 'number') {
  userAge = +prompt('How old are You?', userAge);
}

if (userAge < 18) {
  alert('You are not allowed to visit this website');
} else if (userAge >= 18 && userAge <= 22) {
  confirmAction = confirm('Are you sure you want to continue?');

  if (confirmAction) {
    alert('Welcome, ' + userName);
  } else {
    alert('You are not allowed to visit this website.');
  }
} else {
  alert('Welcome, ' + userName);
}

